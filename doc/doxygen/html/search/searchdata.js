var indexSectionsWithContent =
{
  0: "acdefginoprsuvwx~",
  1: "cdefinpsvx",
  2: "x",
  3: "acdegioprswx~",
  4: "c",
  5: "afirsuw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "enumvalues"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator"
};

