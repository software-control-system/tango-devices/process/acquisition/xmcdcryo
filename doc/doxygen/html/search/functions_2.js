var searchData=
[
  ['data_0',['Data',['../class_x_m_c_d_cryo__ns_1_1_data.html#a705870e1a147a6a2026e4cdaf34a9643',1,'XMCDCryo_ns::Data::Data(long current_iteration, const std::vector&lt; double &gt; &amp;xmcd_spectrum, const std::vector&lt; double &gt; &amp;champ_PLUS_spectrum, const std::vector&lt; double &gt; &amp;champ_MOINS_spectrum)'],['../class_x_m_c_d_cryo__ns_1_1_data.html#ac58dbd741145ba7e5916b2d9cd865c09',1,'XMCDCryo_ns::Data::Data(const Data &amp;src)']]],
  ['delete_5fdevice_1',['delete_device',['../class_x_m_c_d_cryo__ns_1_1_x_m_c_d_cryo.html#ad3262e062c41211211410e9df056b3b6',1,'XMCDCryo_ns::XMCDCryo']]],
  ['dev_5fstate_2',['dev_state',['../class_x_m_c_d_cryo__ns_1_1_x_m_c_d_cryo.html#af9b74868e7822605416f4d0487da5102',1,'XMCDCryo_ns::XMCDCryo']]],
  ['dev_5fstatus_3',['dev_status',['../class_x_m_c_d_cryo__ns_1_1_x_m_c_d_cryo.html#a97c197fc7d492dd2e773bb1bd330925b',1,'XMCDCryo_ns::XMCDCryo']]]
];
