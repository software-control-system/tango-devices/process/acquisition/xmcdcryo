from conan import ConanFile

class XMCDCryoRecipe(ConanFile):
    name = "xmcdcryo"
    executable = "ds_XMCDCryo"
    version = "1.0.0"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Guillaume Pichon"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/process/acquisition/xmcdcryo.git"
    description = "XMCDCryo"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
