/*
 * ControllerState.h
 *
 *  Created on: 9 sept. 2021
 *      Author: pichong
 */

#ifndef SRC_CONTROLLERSTATE_H_
#define SRC_CONTROLLERSTATE_H_

#include <string>
#include <tango.h>

namespace XMCDCryo_ns
{

/**
 * @brief The inner states of the controller
 * 
 */
enum ControllerState
{
    /**
     * @brief The device is initializing.<br/>
     * Tango equivalent state : INIT
     * 
     */
    INITIALIZING,

    /**
     * @brief The device is waiting for instructions.<br/>
     * Tango equivalent state : ON
     * 
     */
    WAITING_INSTRUCTION,

    /**
     * @brief A connection error to another device (CCD or magnet) occured.<br/>
     * Tango equivalent state : FAULT
     * 
     */
    FAULT_CONNECTION,

    /**
     * @brief The CCD is in fault.<br/>
     * Tango equivalent state : FAULT
     * 
     */
    FAULT_CCD,

    /**
     * @brief The magnet is in fault.<br/>
     * Tango equivalent state : FAULT
     * 
     */
    FAULT_MAGNET,

    /**
     * @brief The CCD is unavailable.<br/>
     * Tango equivalent state : DISABLE
     * 
     */
    UNAVAILABLE_CCD,

    /**
     * @brief The magnet is unavailable. It can be in pause or running outside of the control of the XMCDCryo device.<br/>
     * Tango equivalent state : DISABLE
     * 
     */
    UNAVAILABLE_MAGNET,

    /**
     * @brief A start command has been asked by the user.<br/>
     * Tango equivalent state : RUNNING
     * 
     */
    RUNNING_ASKING_FOR_START,

    /**
     * @brief A ramp has been asked to the magnet.<br/>
     * Tango equivalent state : RUNNING
     * 
     */
    RUNNING_RAMP_ASKED,

    /**
     * @brief The magnet is ramping to target.<br/>
     * Tango equivalent state : RUNNING
     * 
     */
    RUNNING_RAMP,

    /**
     * @brief The magnet has reached the target. Waiting before starting CCD acquisition<br/>
     * Tango equivalent state : RUNNING
     * 
     */
    RUNNING_WAITING_AFTER_RAMP,

    /**
     * @brief The CCD is in acquisition<br/>
     * Tango equivalent state : RUNNING
     * 
     */
    RUNNING_CCD_ACQUISITION,
    /**
     * @brief <br/>
     * Tango equivalent state : STANDBY
     * 
     */
    STANDBY_MANUAL_ITERATION,

    /**
     * @brief The final ramp to 0 has been asked to the magnet.<br/>
     * Tango equivalent state : RUNNING
     * 
     */
    RUNNING_ENDING_RAMP_ASKED,

    /**
     * @brief The magnet is ramping to 0.<br/>
     * Tango equivalent state : RUNNING
     * 
     */
    RUNNING_ENDING_RAMP,

    /**
     * @brief The abort of current acquisition has been asked.<br/>
     * Tango equivalent state : RUNNING
     * 
     */
    ABORT_ASKED
};

/**
 * @brief Convert the controller inner state to a string.
 * 
 * @param inner_state The controller inner state
 * @return std::string The controller inner state as a string.
 */
std::string controller_state_to_string(ControllerState inner_state);

/**
 * @brief Returns the description of the inner state in a string.
 * 
 * @param inner_state The controller inner state
 * @return std::string The description of the inner state in a string
 */
std::string controller_state_desciption(ControllerState inner_state);

/**
 * @brief Returns the tango state corresponding to the inner state.
 * 
 * @param inner_state The controller inner state
 * @return Tango::DevState The tango state corresponding to the inner state.
 */
Tango::DevState controller_state_to_tango_state(ControllerState inner_state);


}
#endif //SRC_CONTROLLERSTATE_H_
