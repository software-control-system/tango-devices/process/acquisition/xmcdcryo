/*
 * State.h
 *
 *  Created on: 9 sept. 2021
 *      Author: pichong
 */

#ifndef SRC_STATE_H_
#define SRC_STATE_H_

#include <string>
#include <tango.h>
#include "Parameters.h"

namespace XMCDCryo_ns
{

/**
 * @brief This class contains the XMCD state:<br/>
 * <ul> 
 * <li>Parameters: The parameters used for acquisition</li>
 * <li>State: The Tango state</li>
 * <li>Status: A string describing the state and last events.</li>
 * </ul> 
 * 
 */
class State
{
private:
    /**
     * @brief The parameters used for acquisition
     * 
     */
    Parameters m_parameters;

    /**
     * @brief The Tango state
     * 
     */
	Tango::DevState m_state;

    /**
     * @brief A string describing the state and last events
     * 
     */
	std::string m_status;

public:
    /**
     * @brief Construct a new State object
     * 
     * @param parameters The parameters used for acquisition
     * @param state The Tango state
     * @param status A string describing the state and last events
     */
    State(const Parameters& parameters, const Tango::DevState& state, const std::string& status);

    /**
     * @brief Construct a new State object by copy
     * 
     * @param src The original to copy
     */
	State(const State& src);

    /**
     * @brief Copy operator
     * 
     * @param src The original to copy
     * @return const State& 
     */
    const State & operator= (const State& src);

    /**
     * @brief Get the parameters
     * 
     * @return const Parameters& 
     */
    const Parameters& get_parameters() const;

    /**
     * @brief Get the state
     * 
     * @return Tango::DevState 
     */
	Tango::DevState get_state() const;

    /**
     * @brief Get the status
     * 
     * @return const std::string& 
     */
	const std::string& get_status() const;
};

}

#endif //SRC_STATE_H_
