#include "Computer.h"

namespace XMCDCryo_ns
{

const double ONE_HALF = 1./2.;

Computer::Computer()
{
}

void Computer::add_profile(const std::vector<double>& profile)
{
    if( m_profiles.size()<3 )
    {
        m_profiles.push_back(profile);
    }
    else
    {
        m_profiles[0]=m_profiles[1];
        m_profiles[1]=m_profiles[2];
        m_profiles[2]=profile;
    }
}

const Data& Computer::compute_XMCD_1()
{
    m_last_data.reset();

    //- compute the first spectrum
    size_t n = m_profiles[0].size();

    //- resize buffers for raw data
    m_champ_plus_spectrum.resize(n, 0.0 );
    m_champ_moins_spectrum.resize(n, 0.0 );

    const std::vector<double>& i1 = m_profiles[0];
    const std::vector<double>& i2 = m_profiles[1];
    const std::vector<double>& i3 = m_profiles[2];

    std::vector<double> data_xmcd_spectrum(n, 0.0);
    std::vector<double> data_champ_plus_spectrum(n, 0.0);
    std::vector<double> data_champ_moins_spectrum(n, 0.0);

    for (size_t i = 0; i < n; i++)
    {
        data_xmcd_spectrum[i] = ONE_HALF * ::log( (i1[i] * i3[i]) / (i2[i] * i2[i]) );
        data_champ_plus_spectrum[i] = m_champ_plus_spectrum [i];
        data_champ_moins_spectrum[i] = m_champ_moins_spectrum [i];
    }

    m_last_data.reset(new Data(1, data_xmcd_spectrum, data_champ_plus_spectrum, data_champ_moins_spectrum));
    return *m_last_data;
}

const Data& Computer::compute_XMCD_n(long iteration)
{
    const std::vector<double>& last = this->m_last_data->get_xmcd_spectrum();

    const std::vector<double>& i1 = m_profiles[0];
    const std::vector<double>& i2 = m_profiles[1];
    const std::vector<double>& i3 = m_profiles[2];

    size_t n = m_profiles[0].size();
    m_champ_plus_spectrum.resize(n, 0.0 );
    m_champ_moins_spectrum.resize(n, 0.0 );

    std::vector<double> data_xmcd_spectrum(n, 0.0);
    std::vector<double> data_champ_plus_spectrum(n, 0.0);
    std::vector<double> data_champ_moins_spectrum(n, 0.0);

    for (size_t i = 0; i < n; i++)
    {
        data_xmcd_spectrum[i] = ( (2 * (iteration - 1)) * last[i] + ::log( (i1[i] * i3[i]) / (i2[i] * i2[i]) ) ) / (2 * iteration);
        data_champ_plus_spectrum[i] = m_champ_plus_spectrum [i];
        data_champ_moins_spectrum[i] = m_champ_moins_spectrum [i];
    }

    m_last_data.reset(new Data(iteration, data_xmcd_spectrum, data_champ_plus_spectrum, data_champ_moins_spectrum));
    return *m_last_data;
}

void Computer::set_champ_plus_spectrum(const std::vector<double>& champ_plus_spectrum)
{
    m_champ_plus_spectrum = champ_plus_spectrum;
}

void Computer::set_champ_moins_spectrum(const std::vector<double>& champ_moins_spectrum)
{
    m_champ_moins_spectrum = champ_moins_spectrum;
}

void Computer::clear_data()
{
    m_last_data.reset();
}

void Computer::clear()
{
    //m_last_data.reset();
    m_profiles.clear();
    m_champ_plus_spectrum.clear();
    m_champ_moins_spectrum.clear();
}

}
