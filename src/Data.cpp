#include "Data.h"


namespace XMCDCryo_ns
{

Data::Data(long current_iteration, const std::vector<double>& xmcd_spectrum,
        const std::vector<double>& champ_PLUS_spectrum,
        const std::vector<double>& champ_MOINS_spectrum)
{
    m_current_iteration = current_iteration;
    m_xmcd_spectrum = xmcd_spectrum;
    m_champ_PLUS_spectrum = champ_PLUS_spectrum;
    m_champ_MOINS_spectrum = champ_MOINS_spectrum;
}

Data::Data(const Data& src)
{
    *this = src;
}

/**
 * @brief Copy operator
 * 
 * @param src The original to copy
 * @return const MagnetStatus& 
 */
const Data & Data::operator= (const Data& src)
{
    m_current_iteration = src.m_current_iteration;
    m_xmcd_spectrum = src.m_xmcd_spectrum;
    m_champ_PLUS_spectrum = src.m_champ_PLUS_spectrum;
    m_champ_MOINS_spectrum = src.m_champ_MOINS_spectrum;

    return *this;
}

const long& Data::get_current_iteration() const
{
    return m_current_iteration;
}

const std::vector<double>& Data::get_xmcd_spectrum() const
{
    return m_xmcd_spectrum;
}

const std::vector<double>& Data::get_champ_PLUS_spectrum() const
{
    return m_champ_PLUS_spectrum;
}

const std::vector<double>& Data::get_champ_MOINS_spectrum() const
{
    return m_champ_MOINS_spectrum;
}

}
