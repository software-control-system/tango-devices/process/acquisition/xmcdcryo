/*
 * Computer.h
 *
 *  Created on: 9 sept. 2021
 *      Author: pichong
 */

#ifndef SRC_COMPUTER_H_
#define SRC_COMPUTER_H_

#include <tango.h>
#include <yat/memory/UniquePtr.h>
#include "Data.h"

namespace XMCDCryo_ns
{

/**
 * @brief This class manage the XMCD computation algorithm.
 * 
 */
class Computer {

private:
    /**
     * @brief Pointer to the last data.
     * 
     */
    yat::UniquePtr<Data> m_last_data;

    /**
     * @brief RotList of the 3 last profiles.
     * 
     */
    std::vector<std::vector<double>> m_profiles;

    /**
     * @brief The last CCD acquisition while the field sign was positive
     * 
     */
    std::vector<double> m_champ_plus_spectrum;

    /**
     * @brief The last CCD acquisition while the field sign was negative
     * 
     */
    std::vector<double> m_champ_moins_spectrum;

public:
    /**
     * @brief Construct a new Computer object
     * 
     */
    Computer();

    /**
     * @brief Computation of the first XMCD iteration.
     * 
     * @return const Data& 
     */
    const Data& compute_XMCD_1();

    /**
     * @brief Computation of the XMCD for iterations > 1.
     * 
     * @param iteration the iteration number.
     * @return const Data& 
     */
    const Data& compute_XMCD_n(long iteration);

    /**
     * @brief Add a profil from the CCD
     * 
     * @param profile 
     */
    void add_profile(const std::vector<double>& profile);

    /**
     * @brief Set the CCD acquisition while the field sign was positive
     * 
     * @param champ_plus_spectrum CCD acquisition while the field sign was positive
     */
    void set_champ_plus_spectrum(const std::vector<double>& champ_plus_spectrum);

    /**
     * @brief Set the CCD acquisition while the field sign was negative
     * 
     * @param champ_moins_spectrum CCD acquisition while the field sign was negative
     */
    void set_champ_moins_spectrum(const std::vector<double>& champ_moins_spectrum);

    /**
     * @brief Reset the computer data to restart new iterations. 
     * 
     */
    void clear();

    /**
     * @brief Clears the last data.
     * 
     */
    void clear_data();
};

}

#endif //SRC_COMPUTER_H_
