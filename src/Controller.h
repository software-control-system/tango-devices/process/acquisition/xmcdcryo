/*
 * Controller.h
 *
 *  Created on: 9 sept. 2021
 *      Author: pichong
 */

#ifndef SRC_CONTROLLER_H_
#define SRC_CONTROLLER_H_

#include <tango.h>
#include <yat/memory/UniquePtr.h>
#include <yat4tango/DeviceTask.h>
#include <yat4tango/DeviceProxyHelper.h>

#include "Configuration.h"
#include "State.h"
#include "Data.h"
#include "ControllerState.h"
#include "CommandTimer.h"
#include "Computer.h"

namespace XMCDCryo_ns
{

const size_t CONTROLLER_TASK_PERIODIC_MS = 50; //ms
const size_t CONTROLLER_TASK_TIMEOUT_MS = 5000; //ms

const size_t CONTROLLER_START_MSG = yat::FIRST_USER_MSG + 600;
const size_t CONTROLLER_STOP_MSG = yat::FIRST_USER_MSG + 700;
const size_t CONTROLLER_ITERATE_MSG = yat::FIRST_USER_MSG + 800;

/**
 * @brief This is the main class of the device. Its manage the behaviour.
 * 
 */
class Controller: public yat4tango::DeviceTask
{
  
private:
	/**
	 * @brief Message for running status
	 * 
	 */
	static const std::string RUNNING_STATUS_MSG;

	/**
	 * @brief Message for on status
	 * 
	 */
	static const std::string ON_STATUS_MSG;

	/**
	 * @brief Message for stand-by status
	 * 
	 */
	static const std::string STANDBY_STATUS_MSG;

	/**
	 * @brief Message for fault status
	 * 
	 */
	static const std::string FAULT_STATUS_MSG;

	/**
	 * @brief Message for CCD start acquisition status
	 * 
	 */
	static const std::string CCD_START_COMMAND;

	/**
	 * @brief Pointer to the tango device object
	 * 
	 */
	Tango::Device_4Impl *m_device;

	/**
	 * @brief The inner state
	 * 
	 */
	ControllerState m_inner_state;

	/**
	 * @brief The status
	 * 
	 */
	std::stringstream m_status_message;

	/**
	 * @brief This boolean is true if the status has been retrieve by a get.<br/>
	 * It is set to false when a new message is added.
	 * 
	 */
	bool m_status_retrieved;

	/**
	 * @brief The current field sign of the magnet.
	 * 
	 */
	int m_magnet_sign;

	/**
	 * @brief The XMCD computer
	 * 
	 */
	Computer m_computer;

	/**
	 * @brief The current configuration of the XMCD
	 * 
	 */
    Configuration m_configuration;

	/**
	 * @brief Pointer to the CCD proxy.
	 * 
	 */
	yat::UniquePtr<yat4tango::DeviceProxyHelper> m_proxy_ccd;

	/**
	 * @brief Pointer to the magnet proxy.
	 * 
	 */
	yat::UniquePtr<yat4tango::DeviceProxyHelper> m_proxy_magnet;

	/**
	 * @brief The acquisition parameters
	 * 
	 */
	Parameters m_parameters;

	/**
	 * @brief The last XMCD data
	 * 
	 */
	yat::UniquePtr<Data> m_data;

	/**
	 * @brief A mutex to access to the XMCD data and the device state
	 * 
	 */
	yat::Mutex m_state_and_data_lock;

	/**
	 * @brief A mutex to access to the inner state
	 * 
	 */
	yat::Mutex m_inner_state_lock;

	/**
	 * @brief A mutex to access to the device status
	 * 
	 */
	yat::Mutex m_status_lock;

	/**
	 * @brief This boolean indicates if this initialization is finished
	 * 
	 */
	bool m_initialization_done;

	/**
	 * @brief The current iteration
	 * 
	 */
	long m_current_iteration;

	/**
	 * @brief The cycle number for the current iteration
	 * 
	 */
	long m_iteration_cycle;

	/**
	 * @brief This boolean indicates if iterations are done manualy or not
	 * 
	 */
	bool m_manual_iteration;

	/**
	 * @brief This boolean indicate if the magnet is ready or not to start a ramp.
	 * 
	 */
	bool m_waiting_for_magnet_ramp_delay;

    /**
     * @brief Timer for commands. If not null, it's start when the last command is sent.
     * 
     */
    yat::UniquePtr<CommandTimer> m_timer_command;

    /**
     * @brief Time since the last ramp ends.
     * 
     */
    yat::UniquePtr<yat::Timer> m_timer_last_end_ramp;

	/**
	 * @brief Set the state from proxies states
	 * 
	 */
	void set_state_from_proxies();
	
	/**
	 * @brief Get the state from the ccd
	 * 
	 * @return Tango::DevState 
	 */
	Tango::DevState get_ccd_state();
	
	/**
	 * @brief Get the state from the magnet
	 * 
	 * @return Tango::DevState 
	 */
	Tango::DevState get_magnet_state();

	/**
	 * @brief Conection to the ccd with error management
	 * 
	 * @return true The connection is successfull
	 * @return false The connection failed
	 */
	bool connect_to_ccd();

	/**
	 * @brief Conection to the magnet with error management
	 * 
	 * @return true The connection is successfull
	 * @return false The connection failed
	 */
	bool connect_to_magnet();

	/**
	 * @brief This method is the periodic task which manage the main behaviour
	 * 
	 */
	void periodic_task();

	/**
	 * @brief Get the device state from the inner state
	 * 
	 * @return Tango::DevState 
	 */
	Tango::DevState get_state();

    /**
     * @brief Set the state.
     * 
     */
    void set_inner_state(ControllerState controller_state);

    /**
     * @brief Get the inner state
     * 
     * @return ControllerState The inner state
     */
    ControllerState get_inner_state();

    /**
     * @brief Get the description of the inner state.
     * 
     * @return std::string 
     */
    std::string get_inner_status();

	/**
	 * @brief Append a new message to the status. Before, it clears the status it has been retrieved by a get_status().
	 * 
	 * @param message 
	 */
	void append_status(const std::string& message);

	/**
	 * @brief Get the status and set it as retrieved
	 * 
	 * @return std::string 
	 */
	std::string get_status();

	/**
	 * @brief Sends the configuration to the magnet.
	 * 
	 */
	void configure_magnet_for_iterations();
	
	/**
	 * @brief Launch an acquisition iteration
	 * 
	 */
	void start_iteration();
	
	/**
	 * @brief Sets the magnet to 0.
	 * 
	 */
	void ramp_to_zero();
	
	/**
	 * @brief Ask the magnet to ramp to the given current in amps.
	 * 
	 * @param current 
	 */
	void write_current_to_magnet(double current);
	
	/**
	 * @brief Ask the magnet to ramp to the given field in tesla.
	 * 
	 * @param current 
	 */
	void write_field_to_magnet(double field);
	
	/**
	 * @brief Indicate if the magnet is ready to start a new ramp
	 * 
	 * @return true The magnet is ready to start a new ramp
	 * @return false The minimum delay between two ramp is not passed
	 */
	bool is_magnet_field_can_be_changed();

	/**
	 * @brief Ask the magnet to ramp to the target value in amperes or tesla acording to the command units
	 * 
	 */
	void send_value_to_magnet();

	/**
	 * @brief 
	 * 
	 * @param current 
	 */
	void start_ccd_acquisition();

	/**
	 * @brief Retrieves the profile from the CCD and store it into the XMCD computer
	 * 
	 */
	void retrieving_data_from_ccd();

	/**
	 * @brief Reads the profile from the CCD
	 * 
	 * @return std::vector<double> 
	 */
	std::vector<double> read_ccd_intensity();

protected:
	/**
	 * @brief Inheritate method from the task
	 * 
	 * @param msg 
	 */
	virtual void process_message(yat::Message &msg);

public:
	/**
	 * @brief Construct a new Controller object
	 * 
	 * @param dev The pointer to the Tango device
	 * @param configuration The configuration of the device and the magnet
	 */
	Controller(Tango::Device_4Impl *dev, const Configuration &configuration);
	virtual ~Controller();

	/**
	 * @brief Get the XMCD state
	 * 
	 * @return State* 
	 */
	State* get_XMCD_state();

	/**
	 * @brief Get the XMCD data
	 * 
	 * @return Data* 
	 */
	Data* get_XMCD_data();

	/**
	 * @brief Set the acquisition parameters
	 * 
	 * @param parameters 
	 */
	void set_parameters(const Parameters& parameters);

	/**
	 * @brief Launch acquisition iterations
	 * 
	 */
	void start();

	/**
	 * @brief Stop acquisition iterations
	 * 
	 */
	void stop();

	/**
	 * @brief Execute one iteration and if the device was in "ON" state, it's stating a new manual iterations sequence.
	 * 
	 */
	void iterate();

};

}

#endif //SRC_CONTROLLER_H_
