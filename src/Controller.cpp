#include "Controller.h"


namespace XMCDCryo_ns
{

const int NB_PROXY_RETRY = 5;
const int NB_ACQUISITIONS_FIRST_ITERATION = 3;
const int NB_ACQUISITIONS = 2;

const std::string Controller::RUNNING_STATUS_MSG ("Acquisition is running\n");
const std::string Controller::ON_STATUS_MSG      ("Ready to start a new acquisition");
const std::string Controller::STANDBY_STATUS_MSG ("Acquisition is paused");

Controller::Controller(Tango::Device_4Impl *dev, const Configuration &configuration) :
		yat4tango::DeviceTask(dev), m_configuration(configuration), m_parameters(configuration.get_parameters())
{
	DEBUG_STREAM << "Controller::Controller()" << endl;
	m_device = dev;
	m_current_iteration = 0;
	m_iteration_cycle = 0;
	m_initialization_done = false;
	m_magnet_sign = 1;
	m_manual_iteration = false;
	m_waiting_for_magnet_ramp_delay = false;
	set_timeout_msg_period(CONTROLLER_TASK_TIMEOUT_MS);
	set_periodic_msg_period(CONTROLLER_TASK_PERIODIC_MS);
	enable_timeout_msg(false);
	enable_periodic_msg(false);
}

Controller::~Controller()
{
	m_proxy_ccd.reset();
	m_proxy_magnet.reset();
}

State* Controller::get_XMCD_state()
{
	State* state = NULL;
	{
    	yat::MutexLock scoped_lock(m_state_and_data_lock);
		std::stringstream full_status_message;
		full_status_message << get_inner_status() << std::endl << get_status();
		state = new State(m_parameters, get_state(), full_status_message.str());
	}
	return state;
}

Data* Controller::get_XMCD_data()
{
	Data* data = NULL;
	{
    	yat::MutexLock scoped_lock(m_state_and_data_lock);
		if( m_data.get() )
		{
			data = new Data(*m_data.get());
		}
	}
	return data;
}

void Controller::set_parameters(const Parameters& parameters)
{
	yat::MutexLock scoped_lock(m_state_and_data_lock);
	m_parameters = parameters;
}

void Controller::start()
{
	if( m_initialization_done )
	{
		if( get_state()==Tango::ON || get_state()==Tango::STANDBY )
		{
			set_inner_state(RUNNING_ASKING_FOR_START);
			yat::Message *msg = yat::Message::allocate(CONTROLLER_START_MSG, DEFAULT_MSG_PRIORITY, true);
			post(msg);
			append_status("Start command sent.\n");
		}
		else
		{
			Tango::Except::throw_exception("TANGO_DEVICE_ERROR", "The state must be in ON or STANDBY state.", "Controller::start()");
		}
	}
	else
	{
		Tango::Except::throw_exception("TANGO_DEVICE_ERROR", "The initialization is not done.", "Controller::start()");
	}
}

void Controller::stop()
{
	if( m_initialization_done )
	{
		if( get_state()==Tango::RUNNING )
		{
			set_inner_state(ABORT_ASKED);
			yat::Message *msg = yat::Message::allocate(CONTROLLER_STOP_MSG, DEFAULT_MSG_PRIORITY, true);
			post(msg);
			append_status("Stop command sent.\n");
		}
		else
		{
			Tango::Except::throw_exception("TANGO_DEVICE_ERROR", "The state must be in ON state.", "Controller::stop()");
		}
	}
	else
	{
		Tango::Except::throw_exception("TANGO_DEVICE_ERROR", "The initialization is not done.", "Controller::stop()");
	}
}

void Controller::iterate()
{
	if( m_initialization_done )
	{
		if( get_state()==Tango::STANDBY || get_state()==Tango::ON )
		{
			set_inner_state(RUNNING_ASKING_FOR_START);
			yat::Message *msg = yat::Message::allocate(CONTROLLER_ITERATE_MSG, DEFAULT_MSG_PRIORITY, true);
			post(msg);
			append_status("Iterate command sent.\n");
		}
		else
		{
			Tango::Except::throw_exception("TANGO_DEVICE_ERROR", "The state must be in ON or STANDBY state.", "Controller::iterate()");
		}
	}
	else
	{
		Tango::Except::throw_exception("TANGO_DEVICE_ERROR", "The initialization is not done.", "Controller::iterate()");
	}
}

void Controller::periodic_task()
{
	if( m_initialization_done )
	{
		//Retreiving states
		Tango::DevState ccd_state = get_ccd_state();
		Tango::DevState magnet_state = get_magnet_state();
		ControllerState prev_inner_state = get_inner_state();

		//Managing errors.
		bool fault = false;
		if( ccd_state == Tango::FAULT )
		{
			set_inner_state(FAULT_CCD);
			if( prev_inner_state!=FAULT_CCD )
			{
				std::string message("CCD is in fault.");
				append_status(message);
				ERROR_STREAM << message << std::endl;
			}
		}
		else
		{
			if( magnet_state == Tango::FAULT )
			{
				set_inner_state(FAULT_MAGNET);
				if( prev_inner_state!=FAULT_MAGNET )
				{
					std::string message("Magnet is in fault.");
					append_status(message);
					ERROR_STREAM << message << std::endl;
				}
			}
			else
			{
				if( magnet_state == Tango::DISABLE )
				{
					//If the magnet is in pause while the acquisition it stops it.
					set_inner_state(UNAVAILABLE_MAGNET);
					if( prev_inner_state!=UNAVAILABLE_MAGNET )
					{
						std::string message("Magnet is in pause.");
						append_status(message);
						ERROR_STREAM << message << std::endl;
					}
				}
			}
		}

		ControllerState inner_state = get_inner_state();

		//Managing states events.
		switch(inner_state)
		{
		case INITIALIZING:
		case WAITING_INSTRUCTION:
		case FAULT_CONNECTION:
		case FAULT_CCD:
		case FAULT_MAGNET:
		case UNAVAILABLE_CCD:
		case UNAVAILABLE_MAGNET:
		{
			//In thoses cases, the inner state depends on other devices.
			set_state_from_proxies();
		}
			break;
		case RUNNING_ASKING_FOR_START:
			break;
		case RUNNING_RAMP_ASKED:
		{
			if( magnet_state==Tango::RUNNING )
			{
				//The ramp request is valid and the magnet is actually ramping.
				set_inner_state(RUNNING_RAMP);
				std::string message("Magnet is ramping to target.\n");
				append_status(message);
				INFO_STREAM << message << std::endl;
				m_timer_command.reset();
			}
			else 
			{
				//The magnet is still not ramping. Checking if the timout is passed.
				double ellapsed_seconds = 0.0;
				if( m_timer_command.get() )
				{
					ellapsed_seconds = m_timer_command->get_ellapsed_seconds();
				}

				if( ellapsed_seconds>m_configuration.get_maximum_response_time() )
				{
					//The request is considered as lost. The magnet didn't take it into account.
					set_inner_state(FAULT_CONNECTION);
					std::string error_message("The magnet did not start to ramp.\n");
					append_status(error_message);
					ERROR_STREAM << error_message << std::endl;
				}
			}
		}
			break;
		case RUNNING_RAMP:
		{
			//Checking if the magnet is still ramping. If it goes back to ON, the ramp is done successfully.
			if( magnet_state==Tango::ON )
			{
				set_inner_state(RUNNING_WAITING_AFTER_RAMP);
				m_timer_last_end_ramp.reset(new yat::Timer());
				std::string message("End of the magnet ramp. Waiting before capture.\n");
				append_status(message);
				INFO_STREAM << message << std::endl;
			}
		}
			break;
		case RUNNING_WAITING_AFTER_RAMP:
		{
			double ellapsed_seconds = 0.0;
			bool time_ellapsed = true;
			if( m_timer_last_end_ramp.get() )
			{
				ellapsed_seconds = m_timer_last_end_ramp->elapsed_sec();
			}
			else
			{
				//Wiered state.
				m_timer_last_end_ramp.reset(new yat::Timer());
			}
			if( ellapsed_seconds>=m_parameters.get_delay() )
			{
				start_ccd_acquisition();
				set_inner_state(RUNNING_CCD_ACQUISITION);
				std::string message("Starting CCD acquisition\n");
				append_status(message);
				INFO_STREAM << message << std::endl;
			}
		}
			break;
		case RUNNING_CCD_ACQUISITION:
		{
			//The CCD is in acquisition. Waiting until its not running.
			//Carefull: if the magnet is not ready to start a new ramp, nothing is done.
			if( ccd_state!=Tango::RUNNING )
			{
				if( !m_waiting_for_magnet_ramp_delay ) 
				{
					//End of iteration acquisition.
					retrieving_data_from_ccd();
					m_magnet_sign = -m_magnet_sign;
					DEBUG_STREAM << "m_iteration_cycle++;" << m_iteration_cycle << "/" << m_current_iteration << std::endl;
					m_iteration_cycle++;
				}
				if( m_current_iteration==0)
				{
					//Specific case for first iteration.
					if( m_iteration_cycle<NB_ACQUISITIONS_FIRST_ITERATION )
					{
						DEBUG_STREAM << "1a start_iteration()" << m_iteration_cycle << "/" << m_current_iteration << std::endl;
						start_iteration();
					}
					else //m_iteration_cycle==NB_ACQUISITIONS_FIRST_ITERATION
					{
						if( !m_waiting_for_magnet_ramp_delay ) 
						{
							//Computiong the first XMCD and going to next iteration.
							Data data = m_computer.compute_XMCD_1();
							m_data.reset(new Data(data));
							DEBUG_STREAM << "1 m_current_iteration++;" << m_iteration_cycle << "/" << m_current_iteration << std::endl;
							m_current_iteration++;
						}

						//Going to next iteration or end.
						if( m_current_iteration<m_parameters.get_nb_iteration() )
						{
							if( !m_waiting_for_magnet_ramp_delay ) 
							{
								m_iteration_cycle=0;
								if( m_manual_iteration )
								{
									set_inner_state(STANDBY_MANUAL_ITERATION);
									std::string message("End of first iteration. Standing by.\n");
									append_status(message);
									INFO_STREAM << message << std::endl;
								}
								else
								{
									DEBUG_STREAM << "1b start_iteration()" << m_iteration_cycle << "/" << m_current_iteration << std::endl;
									start_iteration();
								}
							}
						}
						else
						{
							ramp_to_zero();
							DEBUG_STREAM << "1 ramp_to_zero()" << m_iteration_cycle << "/" << m_current_iteration << std::endl;
							if( !m_waiting_for_magnet_ramp_delay ) 
							{
								m_current_iteration = 0;
								m_iteration_cycle = 0;
								m_magnet_sign = 1;
							}
						}
					}
				}
				else
				{
					if( m_iteration_cycle<NB_ACQUISITIONS )
					{
						DEBUG_STREAM << "2a start_iteration()" << m_iteration_cycle << "/" << m_current_iteration << std::endl;
						start_iteration();
					}
					else //m_iteration_cycle==NB_ACQUISITIONS
					{
						if( !m_waiting_for_magnet_ramp_delay ) 
						{
							Data data = m_computer.compute_XMCD_n(m_current_iteration+1);
							m_data.reset(new Data(data));
							DEBUG_STREAM << "2 m_current_iteration++;" << m_iteration_cycle << "/" << m_current_iteration << std::endl;
							m_current_iteration++;
						}

						//Going to next iteration or end.
						if( m_current_iteration<m_parameters.get_nb_iteration() )
						{
							if( !m_waiting_for_magnet_ramp_delay ) 
							{
								m_iteration_cycle=0;
								if( m_manual_iteration )
								{
									set_inner_state(STANDBY_MANUAL_ITERATION);
									std::string message("End of iteration. Standing by.\n");
									append_status(message);
									INFO_STREAM << message << std::endl;
								}
								else
								{
									DEBUG_STREAM << "2b start_iteration()" << m_iteration_cycle << "/" << m_current_iteration << std::endl;
									start_iteration();
								}
							}
						}
						else
						{
							DEBUG_STREAM << "2 ramp_to_zero()" << m_iteration_cycle << "/" << m_current_iteration << std::endl;
							ramp_to_zero();
							if( !m_waiting_for_magnet_ramp_delay ) 
							{
								//m_computer.clear();
								m_current_iteration = 0;
								m_iteration_cycle = 0;
								m_magnet_sign = 1;
							}
						}
					}
				}
			}
		}
			break;
		case STANDBY_MANUAL_ITERATION:
			//Nothing to do, waiting for user command.
			break;
		case RUNNING_ENDING_RAMP_ASKED:
		{
			if( magnet_state==Tango::RUNNING )
			{
				set_inner_state(RUNNING_ENDING_RAMP);
				std::string message("Magnet has start ramping to 0.\n");
				append_status(message);
				INFO_STREAM << message << std::endl;
				m_timer_command.reset();
			}
			else 
			{
				double ellapsed_seconds = 0.0;
				if( m_timer_command.get() )
				{
					ellapsed_seconds = m_timer_command->get_ellapsed_seconds();
				}

				if( ellapsed_seconds>m_configuration.get_maximum_response_time() )
				{
					set_inner_state(FAULT_CONNECTION);
					std::string error_message("The magnet did not start to ramp.\n");
					append_status(error_message);
					ERROR_STREAM << error_message << std::endl;
				}
			}
		}
			break;
		case RUNNING_ENDING_RAMP:
		{
			//Checking if the magnet is still ramping. If it goes back to ON, the ramp is done successfully.
			if( magnet_state==Tango::ON )
			{
				set_inner_state(WAITING_INSTRUCTION);
				std::string message("End of the magnet ramp. Waiting for command.\n");
				append_status(message);
				INFO_STREAM << message << std::endl;
			}
		}
			break;
		case ABORT_ASKED:
			if( magnet_state==Tango::ON )
			{
				if( !m_waiting_for_magnet_ramp_delay ) 
				{
					append_status("Aborting.");
					INFO_STREAM << "Aborting." << std::endl;
				}
				ramp_to_zero();
			}
			break;
		};
	}
}

void Controller::append_status(const std::string& message)
{
	yat::MutexLock scoped_lock(m_status_lock);
	if( m_status_retrieved )
	{
		m_status_message.str(message);
	}
	else
	{
		m_status_message << message;
	}
	m_status_retrieved = false;
}

std::string Controller::get_status()
{
	yat::MutexLock scoped_lock(m_status_lock);
	m_status_retrieved = true;
	return m_status_message.str();
}

bool Controller::connect_to_ccd()
{
	bool connected = false;
	try
	{
		INFO_STREAM << "   Connection to " << m_configuration.get_ccd_proxy() << std::endl;
		m_proxy_ccd.reset(new yat4tango::DeviceProxyHelper(m_configuration.get_ccd_proxy(), m_device));
		m_proxy_ccd->get_device_proxy()->ping();
		INFO_STREAM << "   Connection to " << m_configuration.get_ccd_proxy() << " succeed." << std::endl;
		connected = true;
	}
	catch (const std::bad_alloc &e)
	{
		std::stringstream status;
		ERROR_STREAM << e.what() << ENDLOG;
		status << "Unable to create proxy to ( " << m_configuration.get_ccd_proxy() << " ) :\n"
				<< "--> Memory allocation exception.\n" << endl;
		append_status(status.str());
		INFO_STREAM << "   Connection to " << m_configuration.get_ccd_proxy() << " failed." << std::endl;
	}
	catch (Tango::DevFailed &e)
	{
		std::stringstream status;
		ERROR_STREAM << e << ENDLOG;
		status << "Unable to create proxy to ( " << m_configuration.get_ccd_proxy() << " ) :\n"
				<< "--> check the value of the 'CCDProxy' property.\n"
				<< "--> check the state of the device ( " << m_configuration.get_ccd_proxy() << " ).\n" << std::endl;
		append_status(status.str());
		INFO_STREAM << "   Connection to " << m_configuration.get_ccd_proxy() << " failed." << std::endl;
	}
	return connected;
}

bool Controller::connect_to_magnet()
{
	bool connected = false;
	try
	{
		INFO_STREAM << "   Connection to " << m_configuration.get_magnet_proxy() << std::endl;
		m_proxy_magnet.reset(new yat4tango::DeviceProxyHelper(m_configuration.get_magnet_proxy(), m_device));
		m_proxy_magnet->get_device_proxy()->ping();
		INFO_STREAM << "   Connection to " << m_configuration.get_magnet_proxy() << " succeed. " << std::endl;
		connected = true;
	}
	catch (const std::bad_alloc &e)
	{
		ERROR_STREAM << e.what() << ENDLOG;
		std::stringstream status;
		status << "Unable to create proxy to ( " << m_configuration.get_magnet_proxy() << " ) :\n"
				<< "--> Memory allocation exception.\n" << endl;
		append_status(status.str());
		INFO_STREAM << "   Connection to " << m_configuration.get_magnet_proxy() << " failed." << std::endl;
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << ENDLOG;
		std::stringstream status;
		status << "Unable to create proxy to ( " << m_configuration.get_magnet_proxy() << " ) :\n"
				<< "--> check the value of the 'MagFieldProxy' property.\n"
				<< "--> check the state of the device ( " << m_configuration.get_magnet_proxy() << " ).\n" << std::endl;
		append_status(status.str());
		INFO_STREAM << "   Connection to " << m_configuration.get_magnet_proxy() << " failed." << std::endl;
	}
	return connected;
}

void Controller::process_message(yat::Message &msg)
{
	DEBUG_STREAM << "Controller::process_message(): entering... !" << std::endl;
	try
	{
		switch (msg.type())
		{
		//-----------------------------------------------------
		case yat::TASK_INIT:
		{
			INFO_STREAM << " " << std::endl;
			INFO_STREAM << "--------------------------------------------" << std::endl;
			INFO_STREAM << "-> Controller::TASK_INIT" << std::endl;
			INFO_STREAM << "--------------------------------------------" << std::endl;
			//Instantiate the hardware depending on the model name.
			enable_periodic_msg(true);
			m_initialization_done = connect_to_ccd();
			m_initialization_done &= connect_to_magnet();
			set_state_from_proxies();
			INFO_STREAM << "--------------------------------------------" << std::endl;

		}
			break;
			//-----------------------------------------------------
		case yat::TASK_EXIT:
		{
			INFO_STREAM << " " << std::endl;
			INFO_STREAM << "--------------------------------------------" << std::endl;
			INFO_STREAM << "-> Controller::TASK_EXIT" << std::endl;
			INFO_STREAM << "--------------------------------------------" << std::endl;
		}
			break;
			//-----------------------------------------------------
		case yat::TASK_TIMEOUT:
		{
			INFO_STREAM << " " << std::endl;
			INFO_STREAM << "--------------------------------------------" << std::endl;
			INFO_STREAM << "-> Controller::TASK_TIMEOUT" << std::endl;
			INFO_STREAM << "--------------------------------------------" << std::endl;
		}
			break;
			//-----------------------------------------------------------------------------------------------

		case yat::TASK_PERIODIC:
		{
			DEBUG_STREAM << " " << std::endl;
			DEBUG_STREAM << "--------------------------------------------" << std::endl;
			DEBUG_STREAM << "-> Controller::TASK_PERIODIC" << std::endl;
			DEBUG_STREAM << "--------------------------------------------" << std::endl;
			periodic_task();
		}
			break;
			//-----------------------------------------------------------------------------------------------

		case CONTROLLER_START_MSG:
		{
			DEBUG_STREAM << " " << std::endl;
			DEBUG_STREAM << "--------------------------------------------" << std::endl;
			DEBUG_STREAM << "-> Controller::CONTROLLER_START_MSG" << std::endl;
			DEBUG_STREAM << "--------------------------------------------" << std::endl;
			append_status(RUNNING_STATUS_MSG);
			if( get_inner_state()==WAITING_INSTRUCTION )
			{
				m_data.reset();
				m_computer.clear();
				configure_magnet_for_iterations();
			}
			m_manual_iteration = false;
			start_iteration();
		}
			break;
			//-----------------------------------------------------------------------------------------------

		case CONTROLLER_STOP_MSG:
		{
			DEBUG_STREAM << " " << std::endl;
			DEBUG_STREAM << "--------------------------------------------" << std::endl;
			DEBUG_STREAM << "-> Controller::CONTROLLER_STOP_MSG" << std::endl;
			DEBUG_STREAM << "--------------------------------------------" << std::endl;
			set_inner_state(ABORT_ASKED);
			std::stringstream message;
			message << "Aborting." << std::endl;
			append_status(message.str());
			INFO_STREAM << message << std::endl;
		}
			break;
			//-----------------------------------------------------------------------------------------------

		case CONTROLLER_ITERATE_MSG:
		{
			DEBUG_STREAM << " " << std::endl;
			DEBUG_STREAM << "--------------------------------------------" << std::endl;
			DEBUG_STREAM << "-> Controller::CONTROLLER_ITERATE_MSG" << std::endl;
			DEBUG_STREAM << "--------------------------------------------" << std::endl;
			if( get_inner_state()==WAITING_INSTRUCTION )
			{
				m_data.reset();
				m_computer.clear();
				configure_magnet_for_iterations();
			}
			m_manual_iteration = true;
			start_iteration();
		}
			break;
			//-----------------------------------------------------------------------------------------------
		}
	}
	catch (yat::Exception &ex)
	{
		//- TODO Error Handling
		//ex.dump();
		std::stringstream error_msg("");
		ERROR_STREAM << "Exception from - process_message() : " << endl;
		for (unsigned i = 0; i < ex.errors.size(); i++)
		{
			error_msg << "Origin\t: " << ex.errors[i].origin << endl;
			error_msg << "Desc\t: " << ex.errors[i].desc << endl;
			error_msg << "Reason\t: " << ex.errors[i].reason << endl;
			ERROR_STREAM << "Origin\t: " << ex.errors[i].origin << endl;
			ERROR_STREAM << "Desc\t: " << ex.errors[i].desc << endl;
			ERROR_STREAM << "Reason\t: " << ex.errors[i].reason << endl;
		}

		Tango::Except::throw_exception("TANGO_DEVICE_ERROR", error_msg.str().c_str(), "Controller::process_message()");
	}
}

void Controller::configure_magnet_for_iterations()
{
    Tango::DeviceAttribute hysteresis_attr("enableHysteresis", m_parameters.get_enable_hysteresis());
    m_proxy_magnet->write_attribute(hysteresis_attr);
    Tango::DeviceAttribute var_attr("variation", m_parameters.get_variation());
    m_proxy_magnet->write_attribute(var_attr);
    Tango::DeviceAttribute var_field_attr("variationField", m_parameters.get_variation_field());
    m_proxy_magnet->write_attribute(var_field_attr);
}

void Controller::start_iteration()
{
	m_waiting_for_magnet_ramp_delay = !is_magnet_field_can_be_changed();
	if( !m_waiting_for_magnet_ramp_delay )
	{
		send_value_to_magnet();
		set_inner_state(RUNNING_RAMP_ASKED);
		m_timer_command.reset(new CommandTimer(RUNNING_RAMP_ASKED));
	}
}

void Controller::ramp_to_zero()
{
	m_waiting_for_magnet_ramp_delay = !is_magnet_field_can_be_changed();
	if( !m_waiting_for_magnet_ramp_delay )
	{
		write_field_to_magnet(0.0);
		set_inner_state(RUNNING_ENDING_RAMP_ASKED);
		m_timer_command.reset(new CommandTimer(RUNNING_ENDING_RAMP_ASKED));
	}
}

void Controller::send_value_to_magnet()
{
	std::stringstream status;
	if( m_parameters.get_command_units()==TESLA)
	{
		double field = m_parameters.get_field_value()*m_magnet_sign;
		status << "Sending field " << field << "T to magnet." << std::endl;
		write_field_to_magnet(field);
	}
	else
	{
		double current = m_parameters.get_current_value()*m_magnet_sign;
		status << "Sending current " << current << "A to magnet." << std::endl;
		write_current_to_magnet(current);
	}
	INFO_STREAM << status.str();
	append_status(status.str());
}

void Controller::write_current_to_magnet(double current)
{
    Tango::DeviceAttribute current_attr("current", current);
    m_proxy_magnet->write_attribute(current_attr);
}

void Controller::write_field_to_magnet(double field)
{
    Tango::DeviceAttribute field_attr("field", field);
    m_proxy_magnet->write_attribute(field_attr);
}

bool Controller::is_magnet_field_can_be_changed()
{
	Tango::DeviceAttribute remaining_time_before_next_ramp_attr =
			m_proxy_magnet->read_attribute("remainingTimeBeforeNextRamp");
	double remaining_time_before_next_ramp = 0.0;
	remaining_time_before_next_ramp_attr >> remaining_time_before_next_ramp;
	return remaining_time_before_next_ramp <= 0.0001;
}

void Controller::start_ccd_acquisition()
{
    m_proxy_ccd->command_out("Start");
}

void Controller::retrieving_data_from_ccd()
{
	std::vector<double> ccd_intensity = read_ccd_intensity();
	m_computer.add_profile(ccd_intensity);
	if (m_magnet_sign > 0)
	{
		m_computer.set_champ_plus_spectrum(ccd_intensity);
	}
	else
	{
		m_computer.set_champ_moins_spectrum(ccd_intensity);
	}
}

std::vector<double> Controller::read_ccd_intensity()
{
    Tango::DeviceAttribute intensity_attr;

    intensity_attr = m_proxy_ccd->read_attribute("averageIntensity");
    Tango::DevVarDoubleArray* double_array;
	std::vector<double> ccd_intensity;
    intensity_attr >> ccd_intensity;
	return ccd_intensity;
}

//- Set the state.
void Controller::set_inner_state(ControllerState controller_state)
{
	yat::MutexLock scoped_lock(m_inner_state_lock);
	if( controller_state!=m_inner_state )
	{
		INFO_STREAM << "Inner state is " << controller_state_to_string(controller_state) << std::endl;
		m_inner_state = controller_state;
	}
}

ControllerState Controller::get_inner_state()
{
	yat::MutexLock scoped_lock(m_inner_state_lock);
	return m_inner_state;
}


//- Get the device state
std::string Controller::get_inner_status()
{
	std::string innner_status = controller_state_desciption(get_inner_state());
	if( m_waiting_for_magnet_ramp_delay )
	{
		innner_status += "\nWaiting for magnet delay.";
	}
	return innner_status;
}

//- Get the device state
Tango::DevState Controller::get_state()
{
	ControllerState inner_state = get_inner_state();
	Tango::DevState device_state = controller_state_to_tango_state(inner_state);
	return device_state;
}

void Controller::set_state_from_proxies()
{
	ControllerState inner_state = INITIALIZING;

	if (m_initialization_done)
	{
		Tango::DevState ccd_state = get_ccd_state();
		Tango::DevState magnet_state = get_magnet_state();
		bool magnet_field_can_be_changed = is_magnet_field_can_be_changed();

		if( ccd_state == Tango::FAULT )
		{
			inner_state = FAULT_CCD;
		}
		else
		{
			if( ccd_state == Tango::RUNNING )
			{
				inner_state = UNAVAILABLE_CCD;
			}
			else
			{
				if( ccd_state == Tango::INIT )
				{
					inner_state = INITIALIZING;
				}
				else
				{
					inner_state = WAITING_INSTRUCTION;
				}
			}
		}
		if( inner_state!=FAULT_CCD)
		{
			if( magnet_state == Tango::FAULT )
			{
				inner_state = FAULT_MAGNET;
			}
			else
			{
				if( magnet_state == Tango::RUNNING || magnet_state == Tango::DISABLE || !magnet_field_can_be_changed )
				{
					inner_state = UNAVAILABLE_MAGNET;
				}
				else
				{
					if( magnet_state == Tango::INIT )
					{
						inner_state = INITIALIZING;
					}
					else
					{
						inner_state = WAITING_INSTRUCTION;
					}
				}
			}
		}
	}
	else
	{
		inner_state = INITIALIZING;
	}
	set_inner_state(inner_state);
}

Tango::DevState Controller::get_ccd_state()
{
	Tango::DevState state = Tango::UNKNOWN;
	if (m_initialization_done)
	{
		try
		{
			m_proxy_ccd->read_attribute<Tango::DevState>("state", state, NB_PROXY_RETRY);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << ENDLOG;
			ERROR_STREAM << "Proxy[" << m_configuration.get_ccd_proxy() << "]\t\t" << " --> Unable to read state !\n";
			state = Tango::FAULT;
		}
	}
	return state;
}

Tango::DevState Controller::get_magnet_state()
{
	Tango::DevState state = Tango::UNKNOWN;
	if (m_initialization_done)
	{
		try
		{
			m_proxy_magnet->read_attribute<Tango::DevState>("state", state, NB_PROXY_RETRY);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << ENDLOG;
			ERROR_STREAM << "Proxy[" << m_configuration.get_magnet_proxy() << "]\t\t" << " --> Unable to read state !\n";
			state = Tango::FAULT;
		}
	}
	return state;
}

}
