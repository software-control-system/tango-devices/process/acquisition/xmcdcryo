#include "Parameters.h"



namespace XMCDCryo_ns
{

Parameters::Parameters(double delay, double current_value, double variation, double field_value,
    double variation_field, bool enable_hysteresis, long nb_iteration, CommandUnits command_units)
{
    m_delay = delay;
    m_current_value = current_value;
    m_variation = variation;
    m_field_value = field_value;
    m_variation_field = variation_field;
    m_enable_hysteresis = enable_hysteresis;
    m_nb_iteration = nb_iteration;
    m_command_units = command_units;
}

Parameters::Parameters(const Parameters& src)
{
    *this = src;
}

/**
 * @brief Copy operator
 * 
 * @param src The original to copy
 * @return const Parameters& 
 */
const Parameters & Parameters::operator= (const Parameters& src)
{
    m_delay = src.m_delay;
    m_current_value = src.m_current_value;
    m_variation = src.m_variation;
    m_field_value = src.m_field_value;
    m_variation_field = src.m_variation_field;
    m_enable_hysteresis = src.m_enable_hysteresis;
    m_nb_iteration = src.m_nb_iteration;
    m_command_units = src.m_command_units;

    return *this;
}

const double& Parameters::get_delay() const
{
    return m_delay;
}

const double& Parameters::get_current_value() const
{
    return m_current_value;
}

const double& Parameters::get_variation() const
{
    return m_variation;
}

const double& Parameters::get_field_value() const
{
    return m_field_value;
}

const double& Parameters::get_variation_field() const
{
    return m_variation_field;
}

const bool& Parameters::get_enable_hysteresis() const
{
    return m_enable_hysteresis;
}

const long& Parameters::get_nb_iteration() const
{
    return m_nb_iteration;
}

const CommandUnits& Parameters::get_command_units() const
{
    return m_command_units;
}

}
