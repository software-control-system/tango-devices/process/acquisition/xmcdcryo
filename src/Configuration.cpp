#include "Configuration.h"



namespace XMCDCryo_ns
{

Configuration::Configuration(const Parameters& parameters,const std::string& magnet_proxy,
    const std::string& ccd_proxy, const double& maximum_response_time) :
m_parameters(parameters)
{
    m_magnet_proxy = magnet_proxy;
    m_ccd_proxy = ccd_proxy;
    m_maximum_response_time = maximum_response_time;
}

Configuration::Configuration(const Configuration& src)
:m_parameters(src.m_parameters)
{
    m_magnet_proxy = src.m_magnet_proxy;
    m_ccd_proxy = src.m_ccd_proxy;
    m_maximum_response_time = src.m_maximum_response_time;
}

/**
 * @brief Copy operator
 * 
 * @param src The original to copy
 * @return const Configuration& 
 */
const Configuration & Configuration::operator=(const Configuration& src)
{
    m_parameters = src.m_parameters;
    m_magnet_proxy = src.m_magnet_proxy;
    m_ccd_proxy = src.m_ccd_proxy;
    m_maximum_response_time = src.m_maximum_response_time;

    return *this;
}

const std::string& Configuration::get_magnet_proxy() const
{
    return m_magnet_proxy;
}

const std::string& Configuration::get_ccd_proxy() const
{
    return m_ccd_proxy;
}

const Parameters& Configuration::get_parameters() const
{
    return m_parameters;
}

const double& Configuration::get_maximum_response_time() const
{
    return m_maximum_response_time;
}
}
