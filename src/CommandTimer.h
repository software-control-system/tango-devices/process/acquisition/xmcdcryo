/*
 * CommandTimer.h
 *
 *  Created on: 9 sept. 2021
 *      Author: pichong
 */

#ifndef SRC_COMMANDTIMER_H_
#define SRC_COMMANDTIMER_H_

#include <yat/time/Timer.h>
#include "ControllerState.h"

namespace XMCDCryo_ns
{

/**
 * @brief The purpose of this class is to monitor the time spend since a command is sent.
 * 
 */
class CommandTimer
{
public:
    /**
     * @brief Construct a new Command Timer
     * @param command_state The state matching the command sent.
     * 
     */
    CommandTimer(ControllerState command_state);
    
    /**
     * @brief Destroy the Command Timer object
     * 
     */
    ~CommandTimer();

    /**
     * @brief Get the command
     * 
     * @return ControllerState The command
     */
    ControllerState get_command_state();
    
    /**
     * @brief Get the ellapsed time in seconds since the creation of the object.
     * 
     * @return double 
     */
    double get_ellapsed_seconds();

private:
    /**
     * @brief The command
     * 
     */
    ControllerState m_command_state;

    /**
     * @brief Timer starting at the instantiation.
     * 
     */
    yat::Timer m_timer;
};

}

#endif //SRC_COMMANDTIMER_H_
