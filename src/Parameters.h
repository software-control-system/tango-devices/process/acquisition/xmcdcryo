/*
 * Parameters.h
 *
 *  Created on: 9 sept. 2021
 *      Author: pichong
 */

#ifndef SRC_PARAMETERS_H_
#define SRC_PARAMETERS_H_

namespace XMCDCryo_ns
{

/**
 * @brief The available command units:<br/>
 * <ul>
 * <li>AMPS: Amperes</li>
 * <li>TESLA: Tesla</li>
 * </ul>
 * 
 */
enum CommandUnits
{
    AMPS=0,
    TESLA=1
};

/**
 * @brief This class contains the acquisition parameters:<br/>
 * <ul> 
 * <li> The delay after the ramp is done before starting the acquisition.</li>
 * <li> The target value of the output current. In amps.</li>
 * <li> The variation to apply to the current in hysteresis mode.</li>
 * <li> The target value of the output field. In tesla.</li>
 * <li> The variation to apply to the field in hysteresis mode. In tesla.</li>
 * <li> The hysteresis mode status.</li>
 * <li> The number of acquisition iterations.</li>
 * <li> The units and field to use to command the magnet.</li>
 * </ul> 
 * 
 */
class Parameters
{
private:
    /**
     * @brief The delay after the ramp is done before starting the acquisition.<br/>
     * Must be set accordingly with the mimnimum delay between acquisitions.
     * 
     */
    double m_delay;

    /**
     * @brief The target value of the output current. In amps.
     * 
     */
    double m_current_value;

    /**
     * @brief The variation to apply to the current in hysteresis mode.
     * 
     */
    double m_variation;

    /**
     * @brief The target value of the output field. In tesla.
     * 
     */
    double m_field_value;

    /**
     * @brief The variation to apply to the field in hysteresis mode. In tesla.
     * 
     */
    double m_variation_field;

    /**
     * @brief If true, the hysteresis mode is enabled.
     * 
     */
    bool m_enable_hysteresis;

    /**
     * @brief The number of acquisition iterations.
     * 
     */
    long m_nb_iteration;

    /**
     * @brief The units and field to use to command the magnet.
     * 
     */
    CommandUnits m_command_units;

public:
    /**
     * @brief Construct a new Parameters object
     * 
     * @param delay The delay after the ramp is done before starting the acquisition.
     * @param current_value The target value of the output current. In amps.
     * @param variation The variation to apply to the current in hysteresis mode.
     * @param field_value The target value of the output field. In tesla.
     * @param variation_field The variation to apply to the field in hysteresis mode. In tesla.
     * @param enable_hysteresis The hysteresis mode status.
     * @param nb_iteration The number of acquisition iterations.
     * @param command_units The units and field to use to command the magnet.
     */
    Parameters(double delay, double current_value, double variation, double field_value,
        double variation_field, bool enable_hysteresis, long nb_iteration, CommandUnits command_units);

    /**
     * @brief Construct a new Parameters by copy
     * 
     * @param src The original object to copy.
     */
	Parameters(const Parameters& src);

    /**
     * @brief Copy operator
     * 
     * @param src The original to copy
     * @return const Parameters& 
     */
    const Parameters & operator= (const Parameters& src);

    /**
     * @brief Get the delay
     * 
     * @return const double& 
     */
    const double& get_delay() const;

    /**
     * @brief Get the current value
     * 
     * @return const double& 
     */
    const double& get_current_value() const;

    /**
     * @brief Get the variation
     * 
     * @return const double& 
     */
    const double& get_variation() const;

    /**
     * @brief Get the field value
     * 
     * @return const double& 
     */
    const double& get_field_value() const;

    /**
     * @brief Get the variation field
     * 
     * @return const double& 
     */
    const double& get_variation_field() const;

    /**
     * @brief Get the enable hysteresis
     * 
     * @return true 
     * @return false 
     */
    const bool& get_enable_hysteresis() const;

    /**
     * @brief Get the nb iteration
     * 
     * @return const long& 
     */
    const long& get_nb_iteration() const;

    /**
     * @brief Get the command units
     * 
     * @return const CommandUnits& 
     */
    const CommandUnits& get_command_units() const;
};

}


#endif //SRC_PARAMETERS_H_
