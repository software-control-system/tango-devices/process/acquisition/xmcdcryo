#include "State.h"



namespace XMCDCryo_ns
{

State::State(const Parameters& parameters, const Tango::DevState& state, const std::string& status)
:m_parameters(parameters)
{
    m_state = state;
    m_status = status;
}

State::State(const State& src)
:m_parameters(src.m_parameters)
{
    m_state = src.m_state;
    m_status = src.m_status;
}

const State & State::operator= (const State& src)
{
    m_parameters = src.m_parameters;
    m_state = src.m_state;
    m_status = src.m_status;

    return *this;
}

const Parameters& State::get_parameters() const
{
    return m_parameters;
}

Tango::DevState State::get_state() const
{
    return m_state;
}

const std::string& State::get_status() const
{
    return m_status;
}

}
