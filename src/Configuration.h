/*
 * Configuration.h
 *
 *  Created on: 9 sept. 2021
 *      Author: pichong
 */

#ifndef SRC_CONFIGURATION_H_
#define SRC_CONFIGURATION_H_

#include <string>
#include "Parameters.h"

namespace XMCDCryo_ns
{

/**
 * @brief This class contains the initial configuration of the device:<br/>
 * <ul>
 * <li>Parameters: The parameters used for acquisition</li>
 * <li>The reference to the magnet device.</li>
 * <li>The reference to the CCD device.</li>
 * <li>The maximum response time for the devices.</li>
 * </ul>
 * 
 */
class Configuration
{
private:
    /**
     * @brief The parameters used for acquisition
     * 
     */
	Parameters m_parameters;

    /**
     * @brief The reference to the magnet device.
     * 
     */
	std::string m_magnet_proxy;

    /**
     * @brief The reference to the CCD device.
     * 
     */
	std::string m_ccd_proxy;

    /**
     * @brief The maximum response time for the devices.
     * 
     */
    double m_maximum_response_time;

public:
    /**
     * @brief Construct a new Configuration object
     * 
     * @param parameters The parameters used for acquisition
     * @param magnet_proxy The reference to the magnet device.
     * @param ccd_proxy The reference to the ccd device.
     * @param maximum_response_time The maximum response time for the devices.
     */
    Configuration(const Parameters& parameters, const std::string& magnet_proxy, const std::string& ccd_proxy, const double& maximum_response_time);

    /**
     * @brief Construct a new Configuration object by copy
     * 
     * @param src The original to copy
     */
	Configuration(const Configuration& src);

    /**
     * @brief Copy operator
     * 
     * @param src The original to copy
     * @return const Configuration& 
     */
    const Configuration & operator= (const Configuration& src);

    /**
     * @brief Get the magnet proxy
     * 
     * @return const std::string& 
     */
	const std::string& get_magnet_proxy() const;

    /**
     * @brief Get the ccd proxy
     * 
     * @return const std::string& 
     */
	const std::string& get_ccd_proxy() const;

    /**
     * @brief Get the parameters
     * 
     * @return const Parameters& 
     */
	const Parameters& get_parameters() const;

    /**
     * @brief Get the maximum response time
     * 
     * @return const double& 
     */
	const double& get_maximum_response_time() const;
};

}

#endif //SRC_CONFIGURATION_H_
