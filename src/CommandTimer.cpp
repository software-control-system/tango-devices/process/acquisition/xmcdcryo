#include "CommandTimer.h"

namespace XMCDCryo_ns
{

//- Constructor
CommandTimer::CommandTimer(ControllerState command_state)
{
	m_command_state = command_state;
}

//- Destructor
CommandTimer::~CommandTimer()
{
}

//- Returns the command
ControllerState CommandTimer::get_command_state()
{
	return m_command_state;
}

//- Returns the time ellapsed since the contruction
double CommandTimer::get_ellapsed_seconds()
{
	return m_timer.elapsed_sec();
}

}
