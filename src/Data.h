/*
 * Data.h
 *
 *  Created on: 9 sept. 2021
 *      Author: pichong
 */

#ifndef SRC_DATA_H_
#define SRC_DATA_H_

#include <vector>

namespace XMCDCryo_ns
{

/**
 * @brief This class represents the result of an XMCD computation:<br/>
 * <ul>
 * <li>The current iteration</li>
 * <li>The last XMCD spectrum</li>
 * <li>The last CCD acquisition while the field sign was positive</li>
 * <li>The last CCD acquisition while the field sign was negative</li>
 * </ul>
 * 
 */
class Data
{
private:
    long m_current_iteration;
    std::vector<double> m_xmcd_spectrum;
    std::vector<double> m_champ_PLUS_spectrum;
    std::vector<double> m_champ_MOINS_spectrum;

public:
    /**
     * @brief Construct a new Data object
     * 
     * @param current_iteration The current iteration
     * @param xmcd_spectrum The last XMCD spectrum
     * @param champ_PLUS_spectrum The last CCD acquisition while the field sign was positive
     * @param champ_MOINS_spectrum The last CCD acquisition while the field sign was negative
     */
    Data(long current_iteration, const std::vector<double>& xmcd_spectrum,
        const std::vector<double>& champ_PLUS_spectrum,
        const std::vector<double>& champ_MOINS_spectrum);

    /**
     * @brief Construct a new Data object
     * 
     * @param src The original to copy
     */
    Data(const Data& src);

    /**
     * @brief Copy operator
     * 
     * @param src The original to copy
     * @return const Data& 
     */
    const Data & operator= (const Data& src);

    /**
     * @brief Get the current iteration
     * 
     * @return const long& 
     */
    const long& get_current_iteration() const;

    /**
     * @brief Get the xmcd spectrum
     * 
     * @return const std::vector<double>& 
     */
    const std::vector<double>& get_xmcd_spectrum() const;

    /**
     * @brief Get the last CCD acquisition while the field sign was positive
     * 
     * @return const std::vector<double>& 
     */
    const std::vector<double>& get_champ_PLUS_spectrum() const;

    /**
     * @brief Get the last CCD acquisition while the field sign was negative
     * 
     * @return const std::vector<double>& 
     */
    const std::vector<double>& get_champ_MOINS_spectrum() const;
};

}


#endif //SRC_DATA_H_
