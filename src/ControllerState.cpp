#include "ControllerState.h"



namespace XMCDCryo_ns
{


std::string controller_state_to_string(ControllerState inner_state)
{
	std::string inner_state_str = "";
	switch (inner_state)
	{
    case INITIALIZING:
		inner_state_str = "INITIALIZING";
		break;
    case WAITING_INSTRUCTION:
		inner_state_str = "WAITING_INSTRUCTION";
		break;
	case FAULT_CONNECTION:
		inner_state_str = "FAULT_CONNECTION";
		break;
	case FAULT_CCD:
		inner_state_str = "FAULT_CCD";
		break;
	case FAULT_MAGNET:
		inner_state_str = "FAULT_MAGNET";
		break;
	case UNAVAILABLE_CCD:
		inner_state_str = "UNAVAILABLE_CCD";
		break;
	case UNAVAILABLE_MAGNET:
		inner_state_str = "UNAVAILABLE_MAGNET";
		break;
	case RUNNING_ASKING_FOR_START:
		inner_state_str = "RUNNING_ASKING_FOR_START";
		break;
	case RUNNING_RAMP_ASKED:
		inner_state_str = "RUNNING_RAMP_ASKED";
		break;
	case RUNNING_RAMP:
		inner_state_str = "RUNNING_RAMP";
		break;
	case RUNNING_WAITING_AFTER_RAMP:
		inner_state_str = "RUNNING_WAITING_AFTER_RAMP";
		break;
	case RUNNING_CCD_ACQUISITION:
		inner_state_str = "RUNNING_CCD_ACQUISITION";
		break;
	case STANDBY_MANUAL_ITERATION:
		inner_state_str = "STANDBY_MANUAL_ITERATION";
		break;
    case RUNNING_ENDING_RAMP_ASKED:
		inner_state_str = "RUNNING_ENDING_RAMP_ASKED";
		break;
    case RUNNING_ENDING_RAMP:
		inner_state_str = "RUNNING_ENDING_RAMP";
		break;
	case ABORT_ASKED:
		inner_state_str = "ABORT_ASKED";
		break;
	default:
		inner_state_str = "UNKNOWN";
		break;
	}
	return inner_state_str;
}


std::string controller_state_desciption(ControllerState inner_state)
{
	std::string innner_status = "UNKNOWN";
	switch (inner_state)
	{
    case INITIALIZING:
		innner_status = "The device is initializing.";
		break;
    case WAITING_INSTRUCTION:
		innner_status = "Ready to start a new acquisition.";
		break;
    case FAULT_CONNECTION:
		innner_status = "Connection to PSU failed.";
		break;
	case FAULT_CCD:
		innner_status = "CCD device is in fault.";
		break;
	case FAULT_MAGNET:
		innner_status = "Magnet device is in fault.";
		break;
	case UNAVAILABLE_CCD:
		innner_status = "CCD device unavailable.";
		break;
	case UNAVAILABLE_MAGNET:
		innner_status = "Magnet device unavailable.";
		break;
	case RUNNING_ASKING_FOR_START:
		innner_status = "Begin to iterate.";
		break;
	case RUNNING_RAMP_ASKED:
		innner_status = "Acquisition is running: Target field sent to magnet.";
		break;
	case RUNNING_RAMP:
		innner_status = "Acquisition is running: Magnet reaching target field.";
		break;
	case RUNNING_WAITING_AFTER_RAMP:
		innner_status = "Acquisition is running: Target field reached, waiting before acquisition.";
		break;
	case RUNNING_CCD_ACQUISITION:
		innner_status = "Acquisition is running: CCD acquisition";
		break;
	case STANDBY_MANUAL_ITERATION:
		innner_status = "Acquisition is paused";
		break;
    case RUNNING_ENDING_RAMP_ASKED:
		innner_status = "Acquisition is running: Target field sent to magnet for final ramp";
		break;
    case RUNNING_ENDING_RAMP:
		innner_status = "Acquisition is running: Magnet reaching target field for final ramp to 0.";
		break;
    case ABORT_ASKED:
		innner_status = "Abort asked.";
		break;
	default:
		innner_status = "State undefined.";
		break;
	}
	return innner_status;
}

Tango::DevState controller_state_to_tango_state(ControllerState inner_state)
{
	Tango::DevState device_state = Tango::UNKNOWN;
	switch (inner_state)
	{
    case INITIALIZING:
		device_state = Tango::INIT;
		break;
    case WAITING_INSTRUCTION:
		device_state = Tango::ON;
		break;
	case FAULT_CONNECTION:
	case FAULT_CCD:
	case FAULT_MAGNET:
		device_state = Tango::FAULT;
		break;
	case UNAVAILABLE_CCD:
	case UNAVAILABLE_MAGNET:
		device_state = Tango::DISABLE;
		break;
	case ABORT_ASKED:
	case RUNNING_ASKING_FOR_START:
	case RUNNING_RAMP_ASKED:
	case RUNNING_RAMP:
	case RUNNING_WAITING_AFTER_RAMP:
	case RUNNING_CCD_ACQUISITION:
	case RUNNING_ENDING_RAMP_ASKED:
	case RUNNING_ENDING_RAMP:
		device_state = Tango::RUNNING;
		break;
	case STANDBY_MANUAL_ITERATION:
		device_state = Tango::STANDBY;
		break;
	default:
		device_state = Tango::UNKNOWN;
		break;
	}
	return device_state;
}

}
